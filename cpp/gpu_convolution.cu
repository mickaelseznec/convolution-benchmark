#include "convolutions.h"

#include <stddef.h>
#include <assert.h>
#include <iostream>
#include <vector>

#include <cuComplex.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <cudnn.h>
#include <cufft.h>
#include <cufftXt.h>
#include <helper_cuda.h>
#include <npp.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <helper_cuda.h>
#include <cuda_profiler_api.h>

#include "utils.h"
#include "gpu_utils.h"

namespace conv {

#define checkCUDNN(expression)                             \
{                                                          \
    cudnnStatus_t status = (expression);                   \
    if (status != CUDNN_STATUS_SUCCESS) {                  \
        std::cerr << "Error on line " << __LINE__ << ": "  \
        << cudnnGetErrorString(status) << std::endl;       \
        std::exit(EXIT_FAILURE);                           \
    }                                                      \
}

#define checkCUBLAS(expression)                             \
{                                                          \
    cublasStatus_t status = (expression);                   \
    if (status != CUBLAS_STATUS_SUCCESS) {                  \
        std::cerr << "Error on line " << __LINE__ << ": "  \
        << std::endl;       \
        std::exit(EXIT_FAILURE);                           \
    }                                                      \
}

#define checkCUFFT(expression)                             \
{                                                          \
    cufftResult status = (expression);                   \
    if (status != CUFFT_SUCCESS) {                  \
        std::cerr << "Error on line " << __LINE__ << ": "  \
        << status << std::endl;       \
        std::exit(EXIT_FAILURE);                           \
    }                                                      \
}

template <typename T>
T *gpu_convolution_cudnn(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const size_t radius {mask_width / 2};
    cudnnDataType_t data_type;
    if (sizeof(T) == sizeof(half))
        data_type = CUDNN_DATA_HALF;
    else if (sizeof(T) == sizeof(float))
        data_type = CUDNN_DATA_FLOAT;
    else if (sizeof(T) == sizeof(double))
        data_type = CUDNN_DATA_DOUBLE;

    cudnnHandle_t cudnn;
    cudnnCreate(&cudnn);

    cudnnTensorDescriptor_t input_descriptor;
    checkCUDNN(cudnnCreateTensorDescriptor(&input_descriptor));
    checkCUDNN(cudnnSetTensor4dDescriptor(input_descriptor,
                /*format=*/CUDNN_TENSOR_NHWC,
                /*dataType=*/data_type,
                /*batch_size=*/1,
                /*channels=*/1,
                /*image_height=*/input_height,
                /*image_width=*/input_width));

    cudnnFilterDescriptor_t kernel_descriptor;
    checkCUDNN(cudnnCreateFilterDescriptor(&kernel_descriptor));
    checkCUDNN(cudnnSetFilter4dDescriptor(kernel_descriptor,
                /*dataType=*/data_type,
                /*format=*/CUDNN_TENSOR_NCHW,
                /*out_channels=*/1,
                /*in_channels=*/1,
                /*kernel_height=*/mask_width,
                /*kernel_width=*/mask_width));

    cudnnConvolutionDescriptor_t convolution_descriptor;
    checkCUDNN(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
    checkCUDNN(cudnnSetConvolution2dDescriptor(convolution_descriptor,
                /*pad_height=*/radius,
                /*pad_width=*/radius,
                /*vertical_stride=*/1,
                /*horizontal_stride=*/1,
                /*dilation_height=*/1,
                /*dilation_width=*/1,
                /*mode=*/CUDNN_CROSS_CORRELATION,
                /*computeType=*/data_type));

    int batch_size{0}, channels{0}, height{0}, width{0};
    checkCUDNN(cudnnGetConvolution2dForwardOutputDim(convolution_descriptor,
                input_descriptor,
                kernel_descriptor,
                &batch_size,
                &channels,
                &height,
                &width));

    assert(height == input_height);
    assert(width == input_width);
    assert(batch_size == 1);
    assert(channels == 1);

    cudnnTensorDescriptor_t output_descriptor;
    checkCUDNN(cudnnCreateTensorDescriptor(&output_descriptor));
    checkCUDNN(cudnnSetTensor4dDescriptor(output_descriptor,
                /*format=*/CUDNN_TENSOR_NHWC,
                /*dataType=*/data_type,
                /*batch_size=*/1,
                /*channels=*/1,
                /*image_height=*/input_height,
                /*image_width=*/input_width));

    cudnnConvolutionFwdAlgo_t convolution_algorithm;
    checkCUDNN(
            cudnnGetConvolutionForwardAlgorithm(cudnn,
                input_descriptor,
                kernel_descriptor,
                convolution_descriptor,
                output_descriptor,
                CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
                /*memoryLimitInBytes=*/0,
                &convolution_algorithm));
    std::cerr << convolution_algorithm << std::endl;

    size_t workspace_bytes{0};
    checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn,
                input_descriptor,
                kernel_descriptor,
                convolution_descriptor,
                output_descriptor,
                convolution_algorithm,
                &workspace_bytes));
    // assert(workspace_bytes > 0);

    void* d_workspace{nullptr};
    cudaMalloc(&d_workspace, workspace_bytes);

    int image_bytes = batch_size * channels * height * width * sizeof(T);

    T* d_input{nullptr};
    cudaMalloc(&d_input, image_bytes);
    cudaMemcpy(d_input, h_input, image_bytes, cudaMemcpyHostToDevice);

    T* d_output{nullptr};
    cudaMalloc(&d_output, image_bytes);
    cudaMemset(d_output, 0, image_bytes);

    T* d_mask{nullptr};
    size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    cudaMalloc(&d_mask, mask_bytes);
    cudaMemcpy(d_mask, h_mask, mask_bytes, cudaMemcpyHostToDevice);

    const float alpha = 1.0f, beta = 0.0f;

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();
        checkCUDNN(cudnnConvolutionForward(cudnn,
                    &alpha,
                    input_descriptor,
                    d_input,
                    kernel_descriptor,
                    d_mask,
                    convolution_descriptor,
                    convolution_algorithm,
                    d_workspace,
                    workspace_bytes,
                    &beta,
                    output_descriptor,
                    d_output));
        cudaDeviceSynchronize();
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T* h_output = new T[input_width * input_height];
    cudaMemcpy(h_output, d_output, image_bytes, cudaMemcpyDeviceToHost);

    cudaFree(d_mask);
    cudaFree(d_input);
    cudaFree(d_output);
    cudaFree(d_workspace);

    cudnnDestroyTensorDescriptor(input_descriptor);
    cudnnDestroyTensorDescriptor(output_descriptor);
    cudnnDestroyFilterDescriptor(kernel_descriptor);
    cudnnDestroyConvolutionDescriptor(convolution_descriptor);

    cudnnDestroy(cudnn);
    return h_output;
}

INSTANTIATE(gpu_convolution_cudnn, half);
INSTANTIATE(gpu_convolution_cudnn, float);
INSTANTIATE(gpu_convolution_cudnn, double);

template<typename T>
T *gpu_convolution_npp(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const size_t radius {mask_width / 2};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    T *d_input, *d_mask, *d_output;
    size_t input_pitch, output_pitch;

    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                input_width * sizeof(T), input_height));
    checkCudaErrors(cudaMalloc(&d_mask, mask_bytes));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T), input_height));

    checkCudaErrors(cudaMemcpy2D(d_input, input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_mask, h_mask, mask_bytes, cudaMemcpyHostToDevice));

    NppiSize mask_size = {(int) mask_width, (int) mask_width};
    NppiPoint mask_offset = {(int) radius, (int) radius};
    NppiSize input_size = {(int) input_width, (int) input_height};

    cudaDeviceSynchronize();
    for (size_t i = 0; i <= loop_count; i++) {
        TICK();
        if (sizeof(T) == sizeof(float)) {
            nppiFilter_32f_C1R((float *) d_input, input_pitch,
                    (float *) d_output, output_pitch,
                    input_size,
                    (float *) d_mask, mask_size, mask_offset);
        } else if (sizeof(T) == sizeof(double)) {
            nppiFilter_64f_C1R((double *) d_input, input_pitch,
                    (double *) d_output, output_pitch,
                    input_size,
                    (double *) d_mask, mask_size, mask_offset);
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T *h_output = new T[input_width * input_height];

    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_mask);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_npp, float);
INSTANTIATE(gpu_convolution_npp, double);

template <typename T, typename TR=T>
T *__gpu_convolution_cublas(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    T *d_A_matrix, *d_B_matrix, *d_C_matrix;
    cublasHandle_t cublas_handle;
    TR alpha, beta = 0.0;

    if (sizeof(T) == sizeof(half)) {
        float one = 1.f;
        half* alpha_half=float_to_half(&one,1);
        *((half*) &alpha) = *alpha_half;
        delete alpha_half;
    } else {
        alpha = 1.0;
    }

    const size_t radius {mask_width / 2};
    const size_t M {input_width * input_height};
    const size_t K {mask_width * mask_width};

    TR *h_A_matrix = new TR[M * K];
    for (int x = 0; x < input_width; x++) {
        for (int y = 0; y < input_height; y++) {
            for (int i = 0; i < K; i++) {
                int y_src = y - radius + (i / mask_width);
                int x_src = x - radius + (i % mask_width);
                if (y_src < 0 || x_src < 0 || y_src >= input_height || x_src >= input_width) {
                    h_A_matrix[i * M + (y * input_width + x)] = 0.0;
                } else {
                    h_A_matrix[i * M + (y * input_width + x)] = *((TR *) &h_input[y_src * input_width + x_src]);
                }
            }
        }
    }

    checkCudaErrors(cudaMalloc(&d_A_matrix, M * K * sizeof(T)));
    checkCudaErrors(cudaMalloc(&d_B_matrix, K * 1 * sizeof(T)));
    checkCudaErrors(cudaMalloc(&d_C_matrix, M * 1 * sizeof(T)));
    checkCudaErrors(cudaMemset(d_C_matrix, 0, M * 1 * sizeof(T)));


    checkCUBLAS(cublasCreate(&cublas_handle));

    checkCUBLAS(cublasSetMathMode(cublas_handle,CUBLAS_TENSOR_OP_MATH)); //allow tensor core use

    checkCUBLAS(cublasSetMatrix(M, K, sizeof(T), h_A_matrix, M, d_A_matrix, M));

    checkCUBLAS(cublasSetMatrix(K, 1, sizeof(T), h_mask, K, d_B_matrix, K));

    std::cout << "k = " << K << std::endl;
    std::cout << "lda = " << M << std::endl;
    std::cout << "ldb = " << K << std::endl;
    std::cout << "ldc = " << M << std::endl;

            cudaProfilerStart();
    for (size_t i = 0; i <= loop_count; i++) {
        TICK();
        if (sizeof(T) == sizeof(float)) {
            checkCUBLAS(cublasSgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
                        M, 1, K, (float *) &alpha,
                        (float *) d_A_matrix, M,
                        (float *) d_B_matrix, K,
                        (float *) &beta, (float *) d_C_matrix, M));
        } else if (sizeof(T) == sizeof(double)) {
            checkCUBLAS(cublasDgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
                        M, 1, K, (double *) &alpha,
                        (double *) d_A_matrix, M,
                        (double *) d_B_matrix, K,
                        (double *) &beta, (double *) d_C_matrix, M));
        } else if (sizeof(T) == sizeof(half)) {
            checkCUBLAS(cublasHgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
                        M, 1, K, (half *) &alpha,
                        (half *) d_A_matrix, M,
                        (half *) d_B_matrix, K,
                        (half *) &beta, (half *) d_C_matrix, M));
        }
        cudaDeviceSynchronize();
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }
            cudaProfilerStop();

    delete[] h_A_matrix;
    T *h_C_matrix = new T[M];
    checkCUBLAS(cublasGetMatrix(M, 1, sizeof(T), d_C_matrix, M, h_C_matrix, M));

    cudaFree(d_A_matrix);
    cudaFree(d_B_matrix);
    cudaFree(d_C_matrix);

    return h_C_matrix;
}

template <typename T>
T *gpu_convolution_cublas(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    return __gpu_convolution_cublas<T>(h_input, input_width, input_height, h_mask, mask_width, results, loop_count);
}

INSTANTIATE(gpu_convolution_cublas, float);
INSTANTIATE(gpu_convolution_cublas, double);

template <>
half *gpu_convolution_cublas<half>(const half *__restrict__ h_input, size_t input_width, size_t input_height,
        const half *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
#if defined(GPU_NATIVE_FP16)
    return __gpu_convolution_cublas<half, short>(h_input, input_width, input_height, h_mask, mask_width, results, loop_count);
#else
    std::cerr << "GPU architecture does not support fp16 math" << std::endl;
    return nullptr;
#endif
}

__global__
void fourier_multiply(cufftComplex *a, cufftComplex *b, size_t width, size_t height)
{
    int base_x = (blockIdx.x * blockDim.x) + threadIdx.x;
    int base_y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(base_y >= height || base_x >= width)
        return;

    cufftComplex temp_a = a[base_y * width + base_x];
    cufftComplex temp_b = b[base_y * width + base_x];

    a[base_y * width + base_x] = cuCmulf(temp_a, temp_b);
}

__global__
void fourier_multiply(cufftDoubleComplex *a, cufftDoubleComplex *b, size_t width, size_t height)
{
    int base_x = (blockIdx.x * blockDim.x) + threadIdx.x;
    int base_y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(base_y >= height || base_x >= width)
        return;

    cufftDoubleComplex temp_a = a[base_y * width + base_x];
    cufftDoubleComplex temp_b = b[base_y * width + base_x];

    a[base_y * width + base_x] = cuCmul(temp_a, temp_b);
}

__global__
void fourier_multiply(half2 *a, half2 *b, size_t width, size_t height)
{
#if defined GPU_NATIVE_FP16
    int base_x = (blockIdx.x * blockDim.x) + threadIdx.x;
    int base_y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(base_y >= height || base_x >= width)
        return;

    half2 temp_a = a[base_y * width + base_x];
    half2 temp_b = b[base_y * width + base_x];

    a[base_y * width + base_x] = make_half2(temp_a.x * temp_b.x - temp_a.y * temp_b.y,
            temp_a.x * temp_b.y + temp_a.y * temp_b.x);
#endif
}

template <typename T>
__global__
void pre_normalize(T *in, size_t length, T n)
{
    int index = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (index >= length) {
        return;
    }
    in[index] *= n;
}

template <typename T, typename TC, cudaDataType data_type_in, cudaDataType data_type_out>
T *__gpu_convolution_cufft(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    cufftHandle p_input, p_output;
    T *d_input, *d_mask, *d_output;
    TC *d_input_fourier, *d_mask_fourier;

    const size_t radius {mask_width / 2};

    const size_t padded_width {upper_power_of_two(input_width + mask_width)};
    const size_t padded_height {upper_power_of_two(input_height + mask_width)};
    long long padded_dims[2] = {(long long) padded_height, (long long) padded_width};

    T *h_filled_mask = new T[padded_width * padded_height];
    memset(h_filled_mask, 0, padded_width * padded_height * sizeof(T));

    for (int y = 0; y < mask_width; y++) {
        for (int x = 0; x < mask_width; x++) {
            int x_dest = x < radius ? padded_width - radius + x : x - radius;
            int y_dest = y < radius ? padded_height - radius + y : y - radius;
            h_filled_mask[y_dest * padded_width + x_dest] = h_mask[y * mask_width + x];
        }
    }

    const size_t input_bytes{padded_width * padded_height * sizeof(T)};
    const size_t input_fourier_bytes{(padded_width / 2 + 1) * padded_height * sizeof(TC)};
    size_t worksize[1];

    // Allocate contiguous memory for both image and mask
    checkCudaErrors(cudaMalloc(&d_input, 2 * input_bytes));
    d_mask = d_input + input_bytes / sizeof(T);
    checkCudaErrors(cudaMalloc(&d_input_fourier,  2 * input_fourier_bytes));
    d_mask_fourier = d_input_fourier + input_fourier_bytes / sizeof(TC);
    checkCudaErrors(cudaMalloc(&d_output, input_bytes));

    // Reset memory for 0-padding
    checkCudaErrors(cudaMemset(d_input, 0, 2 * input_bytes));
    checkCudaErrors(cudaMemcpy2D(d_input, padded_width * sizeof(T),
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy2D(d_mask, padded_width * sizeof(T),
                h_mask, mask_width * sizeof(T),
                mask_width * sizeof(T), mask_width,
                cudaMemcpyHostToDevice));
    checkCUFFT(cufftCreate(&p_input));
    checkCUFFT(cufftXtMakePlanMany(p_input, 2, padded_dims,
                NULL, 1, input_bytes / sizeof(T), data_type_in,
                NULL, 1, input_fourier_bytes / sizeof(TC), data_type_out,
                2, worksize, data_type_out));

    checkCUFFT(cufftCreate(&p_output));
    checkCUFFT(cufftXtMakePlanMany(p_output, 2, padded_dims,
                NULL, 1, input_fourier_bytes / sizeof(TC), data_type_out,
                NULL, 1, input_bytes / sizeof(T), data_type_in,
                1, worksize, data_type_out));

    double normalizer_ref = powf(padded_width * padded_height, -0.5f);
    T normalizer = 0;
    if (sizeof(T) == sizeof(half)) {
        float normalizer_float = (float) normalizer_ref;
        half *res = float_to_half(&normalizer_float, 1);
        normalizer = *res;
        delete[] res;
    } else {
        normalizer = (T) normalizer_ref;
    }

    const dim3 block_size(256, 1, 1);
    const dim3 grid_size(ceili<size_t>(padded_width * padded_height * 2, block_size.x), 1, 1);
    pre_normalize<<<grid_size, block_size>>>(d_input,
            padded_width * padded_height * 2, normalizer);

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();
        checkCUFFT(cufftXtExec(p_input, d_input, d_input_fourier, CUFFT_FORWARD));
        const dim3 block_size(256, 1, 1);
        const dim3 grid_size(ceili<size_t>(padded_width / 2 + 1, block_size.x), padded_height, 1);
        fourier_multiply<<<grid_size, block_size>>>(d_input_fourier, d_mask_fourier, padded_width / 2 + 1, padded_height);

        checkCUFFT(cufftXtExec(p_output, d_input_fourier, d_output, CUFFT_INVERSE));
        cudaDeviceSynchronize();
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T *h_output = new T[input_width * input_height];

    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output + radius * padded_width + radius, padded_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cufftDestroy(p_input);
    cufftDestroy(p_output);
    cudaFree(d_input);
    cudaFree(d_input_fourier);
    cudaFree(d_output);
    return h_output;
}

template <typename T>
T *gpu_convolution_cufft(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count) {
    return nullptr;
}

template <>
half *gpu_convolution_cufft<half>(const half *__restrict__ h_input, size_t input_width, size_t input_height,
        const half *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
#if defined(GPU_NATIVE_FP16)
    return __gpu_convolution_cufft<half, half2, CUDA_R_16F, CUDA_C_16F>(h_input,
            input_width, input_height, h_mask, mask_width, results, loop_count);
#else
    std::cerr << "GPU architecture does not support fp16 math" << std::endl;
    return nullptr;
#endif
}

template <>
float *gpu_convolution_cufft<float>(const float *__restrict__ h_input, size_t input_width, size_t input_height,
        const float *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    return __gpu_convolution_cufft<float, cuComplex, CUDA_R_32F, CUDA_C_32F>(h_input,
            input_width, input_height, h_mask, mask_width, results, loop_count);
}

template <>
double *gpu_convolution_cufft<double>(const double *__restrict__ h_input, size_t input_width, size_t input_height,
        const double *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    return __gpu_convolution_cufft<double, cuDoubleComplex, CUDA_R_64F, CUDA_C_64F>(h_input,
            input_width, input_height, h_mask, mask_width, results, loop_count);
}

}
