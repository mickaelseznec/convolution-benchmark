#pragma once

#include <cuda_fp16.h>

#include "convolutions.h"

half *float_to_half(float *h_input, size_t size);
float *half_to_float(half *h_input, size_t size);
double *half_to_double(half *h_input, size_t size);
double *float_to_double(float *h_input, size_t size);

template<typename T>
__device__
inline T max(T a, T b) {
    return a > b ? a : b;
}

template<typename T>
__device__
inline T min(T a, T b) {
    return a < b ? a : b;
}

#if 0
__device__
inline
half2 operator* (const half2 x, const half2 y)
{
#if __CUDA_ARCH__ >= 530
    return __hmul2(x, y);
#endif
}

__device__
inline
half2 operator+ (const half2 x, const half2 y)
{
#if __CUDA_ARCH__ >= 530
    return __hadd2(x, y);
#endif
}
#endif

