#include "convolutions.h"

#include <stddef.h>
#include <assert.h>
#include <iostream>
#include <vector>

#include <cuda_fp16.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>

#include "utils.h"

namespace conv {

#define MAX_MASK_BYTES (0X10000)
__constant__ char c_mask[MAX_MASK_BYTES];

template <bool unroll=false, size_t t_width=0, size_t packet_size=8, typename T>
__global__
void __k_gpu_convolution_prcf(const T *d_input, size_t input_pitch, T *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    const int ROI_WIDTH {unroll ? (int) t_width : (int) arg_width};
    const size_t radius {ROI_WIDTH / 2};
    const int ROI_HEIGHT {int(2 * radius + packet_size)};


    const int base_y {int(((blockIdx.y * blockDim.y) + threadIdx.y) * packet_size)};
    const int base_x {int((blockIdx.x * blockDim.x) + threadIdx.x)};

    T outval[packet_size];
#pragma unroll
    for (int i = 0 ; i < packet_size; i++)
        outval[i] = 0.0f;


#pragma unroll
    for (int y = 0; y < ROI_HEIGHT; y++) {
#pragma unroll
        for (int x = 0; x < ROI_WIDTH; x++) {
            T pixel_value = d_input[(base_y + y) * input_pitch + (base_x + x)];

            const int from = MAX(y - 2 * radius, 0);
            const int to = MIN(y + 1, packet_size);

#pragma unroll
            for (int p = from; p < to; p++) {
                outval[p] += ((T *)c_mask)[MAX(MIN(y, ROI_WIDTH - 1) - p, 0) * ROI_WIDTH + x] * pixel_value;
            }
        }
    }

#pragma unroll
    for (int i = 0; i < packet_size; i++) {
        d_output[(base_y + i) * output_pitch + base_x] = outval[i];
    }
}

template <bool unroll=false, size_t t_width=0, size_t packet_size=8>
__global__
void __k_gpu_convolution_prcf_half(const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    const int ROI_WIDTH {unroll ? (int) t_width : (int) arg_width};
    const size_t radius {ROI_WIDTH / 2};
    const int ROI_HEIGHT {int(2 * radius + packet_size)};

    const int base_y {int(((blockIdx.y * blockDim.y) + threadIdx.y) * packet_size)};
    const int base_x {int((blockIdx.x * blockDim.x) + threadIdx.x)};

    half outval[packet_size];
#pragma unroll
    for (int i = 0 ; i < packet_size; i++)
        outval[i] = half(0);


#pragma unroll
    for (int y = 0; y < ROI_HEIGHT; y++) {
#pragma unroll
        for (int x = 0; x < ROI_WIDTH; x++) {
            half pixel_value = d_input[(base_y + y) * input_pitch + (base_x + x)];

            const int from = MAX(y - 2 * radius, 0);
            const int to = MIN(y + 1, packet_size);

#pragma unroll
            for (int p = from; p < to; p++) {
#if defined(GPU_NATIVE_FP16)
                outval[p] = __hfma(((half *)c_mask)[MAX(MIN(y, ROI_WIDTH - 1) - p, 0) * ROI_WIDTH + x],
                        pixel_value,
                        outval[p]);
#else
                outval[p] = __float2half(fmaf(__half2float(((half *)c_mask)[MAX(MIN(y, ROI_WIDTH - 1) - p, 0) * ROI_WIDTH + x]),
                                __half2float(pixel_value),
                                __half2float(outval[p])));
#endif
            }
        }
    }

#pragma unroll
    for (int i = 0; i < packet_size; i++) {
        d_output[(base_y + i) * output_pitch + base_x] = outval[i];
    }
}

template <bool unroll=false, size_t t_width=0, size_t packet_size=8>
void k_gpu_convolution_prcf(const dim3 grid_size, const dim3 block_size, const float *d_input, size_t input_pitch, float *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    __k_gpu_convolution_prcf<unroll, t_width, packet_size, float><<<grid_size, block_size>>>(d_input,
            input_pitch, d_output, output_pitch, output_height, arg_width);
}

template <bool unroll=false, size_t t_width=0, size_t packet_size=8>
void k_gpu_convolution_prcf(const dim3 grid_size, const dim3 block_size, const double *d_input, size_t input_pitch, double *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    __k_gpu_convolution_prcf<unroll, t_width, packet_size, double><<<grid_size, block_size>>>(d_input,
            input_pitch, d_output, output_pitch, output_height, arg_width);
}

template <bool unroll=false, size_t t_width=0, size_t packet_size=8>
void k_gpu_convolution_prcf(const dim3 grid_size, const dim3 block_size, const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    __k_gpu_convolution_prcf_half<unroll, t_width, packet_size><<<grid_size, block_size>>>(d_input,
            input_pitch, d_output, output_pitch, output_height, arg_width);
}

template <bool unroll=false, size_t t_width=0, typename T>
__global__
void __k_gpu_convolution_naive(const T *d_input, size_t input_pitch, T *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    size_t x = blockIdx.x * blockDim.x + threadIdx.x;
    size_t y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x > output_width || y > output_height)
        return;

    size_t mask_width {unroll ? t_width : arg_width};

    T result = 0.f;

#pragma unroll
    for(size_t my = 0; my < mask_width; ++my) {
#pragma unroll
        for(size_t mx = 0; mx < mask_width; ++mx) {
            size_t mask_index = my * mask_width + mx;
            T image_value = d_input[(y + my) * input_pitch + (x + mx)];
            result += ((T *) c_mask)[mask_index] * image_value;
        }
    }

    d_output[y * output_pitch + x] = result;
}

template <bool unroll, size_t t_width>
__global__
void __k_gpu_convolution_naive_half(const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    size_t x = blockIdx.x * blockDim.x + threadIdx.x;
    size_t y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x > output_width || y > output_height)
        return;

    size_t mask_width {unroll ? t_width : arg_width};

    half result = 0.f;

#pragma unroll
    for(size_t my = 0; my < mask_width; ++my) {
#pragma unroll
        for(size_t mx = 0; mx < mask_width; ++mx) {
            size_t mask_index = my * mask_width + mx;
            half image_value = d_input[(y + my) * input_pitch + (x + mx)];
#if defined(GPU_NATIVE_FP16)
            result = __hfma(((half *) c_mask)[mask_index], image_value, result);
#else
            result = __float2half(fmaf(__half2float(((half *) c_mask)[mask_index]),
                        __half2float(image_value), __half2float(result)));
#endif
        }
    }

    d_output[y * output_pitch + x] = result;
}

template <bool unroll=false, size_t t_width=0>
void k_gpu_convolution_naive(const dim3 grid_size, const dim3 block_size, const float *d_input, size_t input_pitch, float *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    return __k_gpu_convolution_naive<unroll, t_width, float><<<grid_size, block_size>>>(d_input, input_pitch, d_output, output_pitch, output_width, output_height, arg_width);
}

template <bool unroll=false, size_t t_width=0>
void k_gpu_convolution_naive(const dim3 grid_size, const dim3 block_size, const double *d_input, size_t input_pitch, double *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    return __k_gpu_convolution_naive<unroll, t_width, double><<<grid_size, block_size>>>(d_input, input_pitch, d_output, output_pitch, output_width, output_height, arg_width);
}

template <bool unroll=false, size_t t_width=0>
void k_gpu_convolution_naive(const dim3 grid_size, const dim3 block_size, const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    return __k_gpu_convolution_naive_half<unroll, t_width><<<grid_size, block_size>>>(d_input, input_pitch, d_output, output_pitch, output_width, output_height, arg_width);
}

template <typename T>
T *gpu_convolution_naive(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const dim3 BLOCK_SIZE {32, 8, 1};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    const size_t radius {mask_width / 2};
    assert(mask_bytes <= MAX_MASK_BYTES);

    T *d_input, *d_output;
    size_t input_pitch, output_pitch;


    // Allocate memory for image's zeros halo
    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                (input_width + 2 * radius) * sizeof(T),
                input_height + 2 * radius));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T),
                input_height));

    cudaMemset(d_input, 0, input_pitch * (input_height + 2 * radius));
    // Copy the device pointer, offset to center in the halo
    checkCudaErrors(cudaMemcpy2D(d_input + radius * (input_pitch / sizeof(T) + 1), input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(c_mask, h_mask, mask_bytes, 0, cudaMemcpyHostToDevice));

    const dim3 grid_size {ceili<unsigned int>(input_width, BLOCK_SIZE.x), ceili<unsigned int>(input_height, BLOCK_SIZE.y), 1};

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();

#define ADD_CASE_NAIVE(N) case N:\
        k_gpu_convolution_naive<true, N>(grid_size, BLOCK_SIZE, d_input, input_pitch / sizeof(T),\
                d_output, output_pitch / sizeof(T), input_width, input_height); break

        switch(mask_width) {
            ADD_CASE_NAIVE(0);
            ADD_CASE_NAIVE(1);
            ADD_CASE_NAIVE(2);
            ADD_CASE_NAIVE(3);
            ADD_CASE_NAIVE(4);
            ADD_CASE_NAIVE(5);

            default:
            if (i == 0) {
                std::cout << "/!\\ Using not-unrolled version, might be slow" << std::endl;
            }
            k_gpu_convolution_naive(grid_size, BLOCK_SIZE, d_input, input_pitch / sizeof(T),
                    d_output, output_pitch / sizeof(T), input_width, input_height, mask_width);
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T *h_output = new T[input_width * input_height];
    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_naive, half);
INSTANTIATE(gpu_convolution_naive, float);
INSTANTIATE(gpu_convolution_naive, double);

template <typename T>
T *gpu_convolution_prcf(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const dim3 BLOCK_SIZE {32, 8, 1};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    const size_t radius {mask_width / 2};
    assert(mask_bytes <= MAX_MASK_BYTES);

    T *d_input, *d_output;
    size_t input_pitch, output_pitch;


    // Allocate memory for image's zeros halo
    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                (input_width + 2 * radius) * sizeof(T),
                input_height + 2 * radius));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T),
                input_height));

    // Copy the device pointer, offset to center in the halo
    cudaMemset(d_input, 0, input_pitch * (input_height + 2 * radius));
    checkCudaErrors(cudaMemcpy2D(d_input + radius * (input_pitch / sizeof(T) + 1), input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(c_mask, h_mask, mask_bytes, 0, cudaMemcpyHostToDevice));

    const dim3 grid_size {ceili<unsigned int>(input_width, BLOCK_SIZE.x), ceili<unsigned int>(input_height / 8, BLOCK_SIZE.y), 1};

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();

#define ADD_CASE_PRCF(N) case N:\
        k_gpu_convolution_prcf<true, N>(grid_size, BLOCK_SIZE, d_input, input_pitch / sizeof(T),\
                d_output, output_pitch / sizeof(T), input_height); break

        switch(mask_width) {
            ADD_CASE_PRCF(0);
            ADD_CASE_PRCF(1);
            ADD_CASE_PRCF(2);
            ADD_CASE_PRCF(3);
            ADD_CASE_PRCF(4);
            ADD_CASE_PRCF(5);

            default:
            if (i == 0) {
                std::cout << "/!\\ Using not-unrolled version, might be slow" << std::endl;
            }
            k_gpu_convolution_prcf(grid_size, BLOCK_SIZE, d_input, input_pitch / sizeof(T),
                    d_output, output_pitch / sizeof(T), input_height, mask_width); break;
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T *h_output = new T[input_width * input_height];
    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_prcf, half);
INSTANTIATE(gpu_convolution_prcf, float);
INSTANTIATE(gpu_convolution_prcf, double);

}
