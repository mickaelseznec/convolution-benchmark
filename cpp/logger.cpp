#include "logger.h"

#include <stdarg.h>
#include <stdio.h>

Logger::Logger(Level level) :
    level_(level)
{
}

int Logger::info(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    int result = (level_ == Level::info) ? vprintf(format, args) : 0;
    va_end(args);

    return result;
}

int Logger::csv(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    int result = (level_ == Level::csv) ? vprintf(format, args) : 0;
    va_end(args);

    return result;
}
