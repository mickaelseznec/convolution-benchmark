#include "convolutions.h"

#include <stddef.h>
#include <assert.h>
#include <iostream>
#include <vector>

#include <cuda_fp16.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>

#include "utils.h"
#include "gpu_utils.h"

namespace conv {

__constant__ char c_mask[MAX_MASK_BYTES];

template <bool unroll=false, size_t t_width=0, typename T>
__global__
void k_gpu_convolution_naive(const T *d_input, size_t input_pitch, T *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    size_t x = blockIdx.x * blockDim.x + threadIdx.x;
    size_t y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x > output_width || y > output_height)
        return;

    size_t mask_width {unroll ? t_width : arg_width};

    T result = 0.f;

#pragma unroll
    for(size_t my = 0; my < mask_width; ++my) {
#pragma unroll
        for(size_t mx = 0; mx < mask_width; ++mx) {
            size_t mask_index = my * mask_width + mx;
            T image_value = d_input[(y + my) * input_pitch + (x + mx)];
            result += ((T *) c_mask)[mask_index] * image_value;
        }
    }

    d_output[y * output_pitch + x] = result;
}

template <bool unroll=false, size_t t_width=0>
__global__
void k_gpu_convolution_naive_half2(const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_width, size_t output_height, size_t arg_width=0)
{
    size_t x = 2 * (blockIdx.x * blockDim.x + threadIdx.x);
    size_t y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x >= output_width || y >= output_height)
        return;

    size_t mask_width {unroll ? t_width : arg_width};

    half2 result = make_half2(0.f, 0.f);

#pragma unroll
    for(size_t my = 0; my < mask_width; ++my) {
#pragma unroll
        for(size_t mx = 0; mx < mask_width; ++mx) {
            half mask_value  = ((half *) c_mask)[my * mask_width + mx];
            half image_value_1 = d_input[(y + my) * input_pitch + (x + mx)];
            half image_value_2 = d_input[(y + my) * input_pitch + (x + 1 + mx)];
            result = result + make_half2(mask_value, mask_value) * make_half2(image_value_1, image_value_2);
        }
    }

    *((half2 *) (d_output + y * output_pitch + x)) = result;
}

template <typename T>
T *gpu_convolution_naive(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const dim3 BLOCK_SIZE {32, 8, 1};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    const size_t radius {mask_width / 2};
    assert(mask_bytes <= MAX_MASK_BYTES);

    T *d_input, *d_output;
    size_t input_pitch, output_pitch;

    // Allocate memory for image's zeros halo
    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                (input_width + 2 * radius) * sizeof(T),
                input_height + 2 * radius));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T),
                input_height));

    cudaMemset(d_input, 0, input_pitch * (input_height + 2 * radius));
    // Copy the device pointer, offset to center in the halo
    checkCudaErrors(cudaMemcpy2D(d_input + radius * (input_pitch / sizeof(T) + 1), input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(c_mask, h_mask, mask_bytes, 0, cudaMemcpyHostToDevice));

    const dim3 grid_size {ceili<unsigned int>(input_width, BLOCK_SIZE.x), ceili<unsigned int>(input_height, BLOCK_SIZE.y), 1};

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();

#define ADD_CASE_NAIVE(N) case N:\
        k_gpu_convolution_naive<true, N><<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T), \
            d_output, output_pitch / sizeof(T), input_width, input_height, mask_width); break

        switch(mask_width) {
            /* ADD_CASE_NAIVE(3); */
            /* ADD_CASE_NAIVE(5); */
            /* ADD_CASE_NAIVE(7); */
            /* ADD_CASE_NAIVE(9); */
            /* ADD_CASE_NAIVE(11); */
            /* ADD_CASE_NAIVE(15); */
            /* ADD_CASE_NAIVE(19); */
            /* ADD_CASE_NAIVE(23); */
            /* ADD_CASE_NAIVE(29); */
            /* ADD_CASE_NAIVE(35); */

            default:
            k_gpu_convolution_naive<<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),
                    d_output, output_pitch / sizeof(T), input_width, input_height, mask_width);
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T *h_output = new T[input_width * input_height];
    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_naive, half);
INSTANTIATE(gpu_convolution_naive, float);
INSTANTIATE(gpu_convolution_naive, double);

template <typename T>
T *gpu_convolution_naive_half2(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const dim3 BLOCK_SIZE {32, 8, 1};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    const size_t radius {mask_width / 2};
    assert(mask_bytes <= MAX_MASK_BYTES);

    T *d_input, *d_output;
    size_t input_pitch, output_pitch;


    // Allocate memory for image's zeros halo
    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                (input_width + 2 * radius) * sizeof(T),
                input_height + 2 * radius));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T),
                input_height));

    assert((reinterpret_cast<uintptr_t>(d_output) & 0x3) == 0); // Should be aligned on 4
    assert((output_pitch & 0x3) == 0); // Should be aligned on 4

    cudaMemset(d_input, 0, input_pitch * (input_height + 2 * radius));
    // Copy the device pointer, offset to center in the halo
    checkCudaErrors(cudaMemcpy2D(d_input + radius * (input_pitch / sizeof(T) + 1), input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(c_mask, h_mask, mask_bytes, 0, cudaMemcpyHostToDevice));

    const dim3 grid_size {ceili<unsigned int>(input_width, 2 * BLOCK_SIZE.x), ceili<unsigned int>(input_height, BLOCK_SIZE.y), 1};

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();

#define ADD_CASE_NAIVE_HALF2(N) case N:\
        k_gpu_convolution_naive_half2<true, N><<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),\
                d_output, output_pitch / sizeof(T), input_width, input_height); break

        switch(mask_width) {
            ADD_CASE_NAIVE_HALF2(3);
            ADD_CASE_NAIVE_HALF2(5);
            ADD_CASE_NAIVE_HALF2(7);
            ADD_CASE_NAIVE_HALF2(9);
            ADD_CASE_NAIVE_HALF2(11);
            ADD_CASE_NAIVE_HALF2(15);
            ADD_CASE_NAIVE_HALF2(19);
            ADD_CASE_NAIVE_HALF2(23);
            ADD_CASE_NAIVE_HALF2(29);
            ADD_CASE_NAIVE_HALF2(35);

            default:
            k_gpu_convolution_naive_half2<<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),
                    d_output, output_pitch / sizeof(T), input_width, input_height, mask_width);
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }

    T *h_output = new T[input_width * input_height];
    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_naive_half2, half);

}
