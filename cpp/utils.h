#pragma once

#include <math.h>
#include <stddef.h>
#include <fstream>

#ifndef MIN
#define MIN(a, b) ((a) < (b)) ? (a) : (b)
#endif

size_t upper_power_of_two(size_t x);

template <typename T>
T ceili(T a, T b)
{
    return (a + b - 1) / b;
}

template <typename T>
double compare_images(T *first, T *second, size_t width, size_t height,
        T *difference, double local_threshold=INFINITY, double global_threshold=INFINITY);
