if [[ $0 == "-bash" ]]
then
    BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
else
    BASE_PATH="$( cd "$( dirname "$0" )" && pwd )"
fi

CUDA_PATH="/usr/local/cuda-9.2"
LOCAL_LIB="${BASE_PATH}/deps/lib/x64"

[[ ":$PATH:" != *"${CUDA_PATH}/bin"* ]] && export PATH="${CUDA_PATH}/bin:${PATH}"

[[ ":$LD_LIBRARY_PATH:" != *"${LOCAL_LIB}"* ]] && export LD_LIBRARY_PATH="${LOCAL_LIB}:${LD_LIBRARY_PATH}"
[[ ":$LD_LIBRARY_PATH:" != *"${CUDA_PATH}/lib64"* ]] && export LD_LIBRARY_PATH="${CUDA_PATH}/lib64:${LD_LIBRARY_PATH}"

echo PATH=$PATH
echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
