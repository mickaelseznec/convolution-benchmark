#include "convolutions.h"

#include <stddef.h>
#include <assert.h>
#include <cstdio>
#include <vector>

#include "arrayfire.h"
#include <cuda_runtime.h>

#include "utils.h"

namespace conv {

template <typename T, af::convDomain domain>
T *af_convolution_base(const T *__restrict__ input, size_t input_width, size_t input_height,
        const T *__restrict__ mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    af::array ar_input(input_width, input_height, input);
    af::array ar_mask(mask_width, mask_width, mask);

    af::array ar_result;

    cudaDeviceSynchronize();
    for (size_t i = 0; i <= loop_count; i++) {
        TICK();
        ar_result = af::convolve2(ar_input, ar_mask, AF_CONV_DEFAULT, domain);
        cudaDeviceSynchronize();
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }
    assert(ar_result.dims() == af::dim4(input_width, input_height, 1, 1));

    T *result = new T[input_width * input_height];
    ar_result.host(result);

    return result;
}

template <typename T>
T *af_convolution_spatial(const T *__restrict__ input, size_t input_width, size_t input_height,
        const T *__restrict__ mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    if (mask_width > 17)
        return nullptr;

    return af_convolution_base<T, AF_CONV_SPATIAL>(input, input_width, input_height, mask, mask_width, results, loop_count);
}

INSTANTIATE(af_convolution_spatial, float);
INSTANTIATE(af_convolution_spatial, double);

template <typename T>
T *af_convolution_freq(const T *__restrict__ input, size_t input_width, size_t input_height,
        const T *__restrict__ mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    return af_convolution_base<T, AF_CONV_FREQ>(input, input_width, input_height, mask, mask_width, results, loop_count);
}

INSTANTIATE(af_convolution_freq, float);
INSTANTIATE(af_convolution_freq, double);

}
