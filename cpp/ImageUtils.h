#pragma once

#include <assert.h>
#include <stdint.h>
#include "FreeImage.h"

template <typename T>
static inline T clamp(T value, T min, T max)
{
    if (value > min) {
        if (value < max) {
            return value;
        }
        return max;
    }
    return min;
}

template <typename T>
T *ImageUtils_FloatToArray(FIBITMAP *image)
{
    unsigned int width = FreeImage_GetWidth(image);
    unsigned int height = FreeImage_GetHeight(image);
    T *result = new T[width * height];

    unsigned int pitch = FreeImage_GetPitch(image) / sizeof(float);
    float *in_data = (float *) FreeImage_GetBits(image);

    for(size_t y = 0; y < height; y++) {
        for(size_t x = 0; x < width; x++) {
            result[y * width + x] = static_cast<T>(in_data[y * pitch + x]);
        }
    }

    return result;
}

template <typename T>
FIBITMAP *ImageUtils_ArrayToFloat(const T *image, int width, int height)
{
    FIBITMAP *result = FreeImage_AllocateT(FIT_FLOAT, width, height, 8 * sizeof(float));
    unsigned int pitch = FreeImage_GetPitch(result) / sizeof(float);
    float *out_data = (float *) FreeImage_GetBits(result);

    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            out_data[y * pitch + x] = static_cast<float>(image[y * width + x]);
        }
    }

    return result;
}
