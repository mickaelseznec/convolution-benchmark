#include "float16.h"

#include <cuda_fp16.h>
#include <cuda_runtime.h>

#include "utils.h"

__global__
void k_float_to_half(float *input, size_t size, half *output)
{
    size_t index = blockDim.x * blockIdx.x + threadIdx.x;
    output[index] = __float2half(input[index]);
}

__global__
void k_half_to_float(half *input, size_t size, float *output)
{
    size_t index = blockDim.x * blockIdx.x + threadIdx.x;
    output[index] = __half2float(input[index]);
}

half *float_to_half(float *h_input, size_t size)
{
    float *d_input;
    half *d_output;

    cudaMalloc(&d_input, size * sizeof(float));
    cudaMalloc(&d_output, size * sizeof(half));

    cudaMemcpy(d_input, h_input, size * sizeof(float), cudaMemcpyHostToDevice);

    const dim3 block_size(256);
    const dim3 grid_size(ceili<int>(size, 256));

    k_float_to_half<<<grid_size, block_size>>>(d_input, size, d_output);

    half *h_output = new half[size];
    cudaMemcpy(h_output, d_output, size * sizeof(half), cudaMemcpyDeviceToHost);

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

float *half_to_float(half *h_input, size_t size)
{
    half *d_input;
    float *d_output;

    cudaMalloc(&d_input, size * sizeof(half));
    cudaMalloc(&d_output, size * sizeof(float));

    cudaMemcpy(d_input, h_input, size * sizeof(half), cudaMemcpyHostToDevice);

    const dim3 block_size(256);
    const dim3 grid_size(ceili<int>(size, 256));

    k_half_to_float<<<grid_size, block_size>>>(d_input, size, d_output);

    float *h_output = new float[size];
    cudaMemcpy(h_output, d_output, size * sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}
