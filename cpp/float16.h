#pragma once

#include <cuda_fp16.h>

half *float_to_half(float * h_input, size_t size);
float *half_to_float(half * h_input, size_t size);

