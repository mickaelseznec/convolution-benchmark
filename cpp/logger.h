#pragma once

enum class Level {
    csv,
    info
};

class Logger {
public:
    Logger(Level level);

    int info(const char *format, ...);
    int csv(const char *format, ...);

private:
    const Level level_;
};
