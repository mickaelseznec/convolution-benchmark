#include "convolutions.h"

#include <stddef.h>
#include <assert.h>
#include <iostream>
#include <vector>

#include <cuda_fp16.h>
#include <cuda_runtime.h>
#include <cuda_profiler_api.h>
#include <helper_cuda.h>

#include "utils.h"
#include "gpu_utils.h"

namespace conv {

template <bool unroll=false, size_t t_width=0, int packet_size=8, typename T>
__global__
void k_gpu_convolution_prcf(const T *d_input, size_t input_pitch, T *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    const int ROI_WIDTH {unroll ? (int) t_width : (int) arg_width};
    const int radius {ROI_WIDTH / 2};
    const int ROI_HEIGHT {int(ROI_WIDTH - 1 + packet_size)};

    const int base_y {int(((blockIdx.y * blockDim.y) + threadIdx.y) * packet_size)};
    const int base_x {int((blockIdx.x * blockDim.x) + threadIdx.x)};

    T outval[packet_size];
#pragma unroll
    for (int i = 0 ; i < packet_size; i++)
        outval[i] = 0.0f;

#pragma unroll
    for (int y = 0; y < ROI_HEIGHT; y++) {
#pragma unroll
        for (int x = 0; x < ROI_WIDTH; x++) {
            T pixel_value = d_input[(base_y + y) * input_pitch + (base_x + x)];

            const int from = max(y - 2 * radius, 0);
            const int to = min(y + 1, packet_size);

#pragma unroll
            for (int p = from; p < to; p++) {
                outval[p] += ((T *)c_mask)[(min(y, ROI_WIDTH - 1) - (p - from)) * ROI_WIDTH + x] * pixel_value;
            }
        }
    }

#pragma unroll
    for (int i = 0; i < packet_size; i++) {
        d_output[(base_y + i) * output_pitch + base_x] = outval[i];
    }
}

template <bool unroll=false, size_t t_width=0, int packet_size=8>
__global__
void k_gpu_convolution_prcf_mixed(const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    const int ROI_WIDTH {unroll ? (int) t_width : (int) arg_width};
    const int radius {ROI_WIDTH / 2};
    const int ROI_HEIGHT {int(ROI_WIDTH - 1 + packet_size)};

    const int base_y {int(((blockIdx.y * blockDim.y) + threadIdx.y) * packet_size)};
    const int base_x {int((blockIdx.x * blockDim.x) + threadIdx.x)};

    float outval[packet_size];
#pragma unroll
    for (int i = 0 ; i < packet_size; i++)
        outval[i] = 0.0f;

#pragma unroll
    for (int y = 0; y < ROI_HEIGHT; y++) {
#pragma unroll
        for (int x = 0; x < ROI_WIDTH; x++) {
            float pixel_value = __half2float(d_input[(base_y + y) * input_pitch + (base_x + x)]);

            const int from = max(y - 2 * radius, 0);
            const int to = min(y + 1, packet_size);

#pragma unroll
            for (int p = from; p < to; p++) {
                outval[p] += __half2float(((half *)c_mask)[(min(y, ROI_WIDTH - 1) - (p - from)) * ROI_WIDTH + x]) * pixel_value;
            }
        }
    }

#pragma unroll
    for (int i = 0; i < packet_size; i++) {
        d_output[(base_y + i) * output_pitch + base_x] = __float2half(outval[i]);
    }
}

template <bool unroll=false, size_t t_width=0, int packet_size=8>
__global__
void k_gpu_convolution_prcf_half2(const half *d_input, size_t input_pitch, half *d_output, size_t output_pitch, size_t output_height, size_t arg_width=0)
{
    const int ROI_WIDTH {unroll ? (int) t_width : (int) arg_width};
    const int radius {ROI_WIDTH / 2};
    const int ROI_HEIGHT {int(ROI_WIDTH - 1 + packet_size)};

    const int base_y {int(((blockIdx.y * blockDim.y) + threadIdx.y) * packet_size)};
    const int base_x {int(2 * ((blockIdx.x * blockDim.x) + threadIdx.x))};

    half2 outval[packet_size];
#pragma unroll
    for (int i = 0 ; i < packet_size; i++)
        outval[i] = make_half2(0.0f, 0.0f);

#pragma unroll
    for (int y = 0; y < ROI_HEIGHT; y++) {
#pragma unroll
        for (int x = 0; x < ROI_WIDTH; x++) {
            half pixel_value_1 = d_input[(base_y + y) * input_pitch + (base_x + x)];
            half pixel_value_2 = d_input[(base_y + y) * input_pitch + (base_x + 1 + x)];

            const int from = max(y - 2 * radius, 0);
            const int to = min(y + 1, packet_size);

#pragma unroll
            for (int p = from; p < to; p++) {
                half mask_value = ((half *)c_mask)[(min(y, ROI_WIDTH - 1) - (p - from)) * ROI_WIDTH + x];
                outval[p] = outval[p] + make_half2(mask_value, mask_value) * make_half2(pixel_value_1, pixel_value_2);
            }
        }
    }

#pragma unroll
    for (int i = 0; i < packet_size; i++) {
        *((half2 *)(d_output + (base_y + i) * output_pitch + base_x)) = outval[i];
    }
}

template <typename T>
T *gpu_convolution_prcf(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const dim3 BLOCK_SIZE {32, 8, 1};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    const size_t radius {mask_width / 2};
    assert(mask_bytes <= MAX_MASK_BYTES);

    T *d_input, *d_output;
    size_t input_pitch, output_pitch;

    // Allocate memory for image's zeros halo
    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                (input_width + 2 * radius) * sizeof(T),
                input_height + 2 * radius));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T),
                input_height));

    // Copy the device pointer, offset to center in the halo
    cudaMemset(d_input, 0, input_pitch * (input_height + 2 * radius));
    checkCudaErrors(cudaMemcpy2D(d_input + radius * (input_pitch / sizeof(T) + 1), input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(c_mask, h_mask, mask_bytes, 0, cudaMemcpyHostToDevice));

    const dim3 grid_size {ceili<unsigned int>(input_width, BLOCK_SIZE.x), ceili<unsigned int>(input_height, 8 * BLOCK_SIZE.y), 1};

    cudaProfilerStart();
    for (size_t i = 0; i <= loop_count; i++) {
        TICK();

#define ADD_CASE_PRCF(N) case N:\
        k_gpu_convolution_prcf<true, N><<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),\
                d_output, output_pitch / sizeof(T), input_height); break

        switch(mask_width) {
            /* ADD_CASE_PRCF(3); */
            /* ADD_CASE_PRCF(5); */
            /* ADD_CASE_PRCF(7); */
            /* ADD_CASE_PRCF(9); */
            /* ADD_CASE_PRCF(11); */
            /* ADD_CASE_PRCF(15); */
            /* ADD_CASE_PRCF(19); */
            /* ADD_CASE_PRCF(23); */
            /* ADD_CASE_PRCF(29); */
            /* ADD_CASE_PRCF(35); */

            default:
            k_gpu_convolution_prcf<<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),
                    d_output, output_pitch / sizeof(T), input_height, mask_width); break;
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }
    cudaProfilerStop();

    T *h_output = new T[input_width * input_height];
    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_prcf, half);
INSTANTIATE(gpu_convolution_prcf, float);
INSTANTIATE(gpu_convolution_prcf, double);

template <typename T>
T *gpu_convolution_prcf_half2(const T *__restrict__ h_input, size_t input_width, size_t input_height,
        const T *__restrict__ h_mask, size_t mask_width, std::vector<elapsed_time_t> &results, size_t loop_count)
{
    const dim3 BLOCK_SIZE {32, 8, 1};
    const size_t mask_bytes {mask_width * mask_width * sizeof(T)};
    const size_t radius {mask_width / 2};
    assert(mask_bytes <= MAX_MASK_BYTES);

    T *d_input, *d_output;
    size_t input_pitch, output_pitch;

    // Allocate memory for image's zeros halo
    checkCudaErrors(cudaMallocPitch(&d_input, &input_pitch,
                (input_width + 2 * radius) * sizeof(T),
                input_height + 2 * radius));
    checkCudaErrors(cudaMallocPitch(&d_output, &output_pitch,
                input_width * sizeof(T),
                input_height));

    assert((reinterpret_cast<uintptr_t>(d_output) & 0x3) == 0); // Should be aligned on 4
    assert((output_pitch & 0x3) == 0); // Should be aligned on 4

    // Copy the device pointer, offset to center in the halo
    cudaMemset(d_input, 0, input_pitch * (input_height + 2 * radius));
    checkCudaErrors(cudaMemcpy2D(d_input + radius * (input_pitch / sizeof(T) + 1), input_pitch,
                h_input, input_width * sizeof(T),
                input_width * sizeof(T), input_height,
                cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(c_mask, h_mask, mask_bytes, 0, cudaMemcpyHostToDevice));

    const dim3 grid_size {ceili<unsigned int>(input_width, 2 * BLOCK_SIZE.x), ceili<unsigned int>(input_height, 8 * BLOCK_SIZE.y), 1};

    for (size_t i = 0; i <= loop_count; i++) {
        TICK();

#define ADD_CASE_PRCF_HALF2(N) case N:\
        k_gpu_convolution_prcf_half2<true, N><<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),\
                d_output, output_pitch / sizeof(T), input_height); break

    cudaProfilerStart();
        switch(mask_width) {
            /* ADD_CASE_PRCF_HALF2(3); */
            /* ADD_CASE_PRCF_HALF2(5); */
            /* ADD_CASE_PRCF_HALF2(7); */
            /* ADD_CASE_PRCF_HALF2(9); */
            /* ADD_CASE_PRCF_HALF2(11); */
            /* ADD_CASE_PRCF_HALF2(15); */
            /* ADD_CASE_PRCF_HALF2(19); */
            /* ADD_CASE_PRCF_HALF2(23); */
            /* ADD_CASE_PRCF_HALF2(29); */
            /* ADD_CASE_PRCF_HALF2(35); */

            default:
            k_gpu_convolution_prcf_half2<<<grid_size, BLOCK_SIZE>>>(d_input, input_pitch / sizeof(T),
                    d_output, output_pitch / sizeof(T), input_height, mask_width); break;
        }
        checkCudaErrors(cudaDeviceSynchronize());
        TOCK();
        if (i > 0)
            results.push_back(ELAPSED_TIME);
    }
    cudaProfilerStop();

    T *h_output = new T[input_width * input_height];
    checkCudaErrors(cudaMemcpy2D(h_output, input_width * sizeof(T),
                d_output, output_pitch,
                input_width * sizeof(T), input_height,
                cudaMemcpyDeviceToHost));

    cudaFree(d_input);
    cudaFree(d_output);

    return h_output;
}

INSTANTIATE(gpu_convolution_prcf_half2, half);

}
