#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <chrono>
#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <fstream>

#include "FreeImage.h"
#include "ImageUtils.h"
#include "docopt.h"
#include <cuda_profiler_api.h>
#include <cuda_runtime.h>

#include "convolutions.h"
#include "gpu_utils.h"
#include "utils.h"
#include "logger.h"

using namespace conv;

static const char USAGE[] =
R"(Convolution benchmark

   Usage:
     benchmark golden [--image=<image_path>] ([--width=<kernel_width>] | [--kernel=<kernel_path>])
     benchmark test <implementation> [--precision=<precision>] [--image=<image_path>] ([--width=<kernel_width>] | [--kernel=<kernel_path>]) [--loop=<loop_count>] ([--no-save] | [--profile-only]) [--csv]
     benchmark (-h | --help)

   Options:
     -h --help                  Show this screen.
     --precision=<precision>    Float precision [default: single]
     --image=<image_path>       TIFF input image [default: ../images/cameraman.tiff].
     --width=<kernel_width>     Use constant kernel with specified width [default: 3].
     --kernel=<kernel_path>     Use TIFF image as kernel.
     --loop=<loop_count>        Number of iteration of the algorithm [default: 10].
     --profile-only             Don't save any image nor compare with golden.
     --no-save                  Don't save generated image
     --csv                      Only display the mean time then the mean error in csv on the standard output

)";

static const std::map<const std::string, conv_function_tuple> impl_map {
    {"cpu_naive",  std::make_tuple(nullptr, cpu_convolution_naive<float>, cpu_convolution_naive<double>)},
    {"gpu_naive",  std::make_tuple(gpu_convolution_naive<half>, gpu_convolution_naive<float>, gpu_convolution_naive<double>)},
    {"gpu_naive_half2",  std::make_tuple(gpu_convolution_naive_half2<half>, nullptr, nullptr)},
    {"gpu_prcf",   std::make_tuple(gpu_convolution_prcf<half>, gpu_convolution_prcf<float>, gpu_convolution_prcf<double>)},
    {"gpu_prcf_half2", std::make_tuple(gpu_convolution_prcf_half2<half>, nullptr, nullptr)},
    {"gpu_npp",    std::make_tuple(nullptr, gpu_convolution_npp<float>, gpu_convolution_npp<double>)},
    {"gpu_cudnn",  std::make_tuple(gpu_convolution_cudnn<half>, gpu_convolution_cudnn<float>, gpu_convolution_cudnn<double>)},
    {"gpu_cublas", std::make_tuple(gpu_convolution_cublas<half>, gpu_convolution_cublas<float>, gpu_convolution_cublas<double>)},
    {"gpu_cufft",  std::make_tuple(gpu_convolution_cufft<half>, gpu_convolution_cufft<float>, gpu_convolution_cufft<double>)},
    {"af_spatial", std::make_tuple(nullptr, af_convolution_spatial<float>, af_convolution_spatial<double>)},
    {"af_freq",    std::make_tuple(nullptr, af_convolution_freq<float>, af_convolution_freq<double>)},
};

mixed_precision_tuple get_array_from_bitmap(FIBITMAP *image_source, float_precision float_type);
void promote_to_double(mixed_precision_tuple *image_test_float, size_t width, size_t height, float_precision float_type);

int main(int argc, char *argv[])
{
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE,
            { argv + 1, argv + argc },
            true,                           // show help if requested
            "Convolution benchmark 0.1");   // version string

    bool golden_mode {args["golden"].asBool()};
    bool profile_only {args["--profile-only"].asBool()};
    bool no_save {args["--no-save"].asBool()};
    bool csv_output {args["--csv"].asBool()};
    Logger logger(csv_output ? Level::csv : Level::info);

    std::string image_path {args["--image"].asString()};
    if (image_path.length() < 4 ||
            image_path.compare(image_path.length() - 5, image_path.length(), ".tiff")) {
        std::cerr << "Please use .tiff file as input" << std::endl;
        exit(0);
    }
    logger.info("Using image path:\t\t%s\n", image_path.c_str());
    bool save_error {args["--errorImage"]};
    std::string save_error_img_path;
    if (save_error) {
        save_error_img_path = args["--errorImage"].asString();
    }

    bool default_mask {!args["--kernel"]};
    std::string mask_path;
    size_t mask_width {(size_t) args["--width"].asLong()};
    if (default_mask) {
        logger.info("Using kernel's width:\t\t%lu\n", mask_width);
    } else {
        mask_path = args["--kernel"].asString();
    }

    size_t loop_count {(size_t) args["--loop"].asLong()};
    if (!golden_mode) {
        logger.info("Using:\t\t\t\t%d iterations\n", loop_count);
    }

    std::string implementation;
    float_precision float_type {single_float};
    bool all_mode {false};
    if (!golden_mode) {
        implementation = args["<implementation>"].asString();
        all_mode = implementation == std::string("all");

        if (all_mode) {
            profile_only = true;
        } else if (impl_map.find(implementation) == impl_map.end()) {
            std::cerr << std::endl << "Implementation \"" << implementation << "\" not found." << std::endl;
            std::cerr << "Please use one of the following:" << std::endl;
            for (auto const& impl: impl_map) {
                std::cerr << impl.first << " ";
            }
            std::cerr << std::endl;
            exit(0);
        }
        logger.info("Using implementation:\t\t%s\n", implementation.c_str());
        std::string precision {args["--precision"].asString()};
        if (precision == std::string("single")) {
            float_type = single_float;
        } else if (precision == std::string("double")) {
            float_type = double_float;
        } else if (precision == std::string("half")) {
            float_type = half_float;
        } else {
            logger.info("Error: %s unknown precisions. Defaulting to single_float\n", precision.c_str());
            float_type = single_float;
        }
        logger.info("Using precision:\t\t%s\n", precision.c_str());
    }

    FreeImage_Initialise(TRUE);

    size_t mask_size {mask_width * mask_width};
    mixed_precision_tuple mask;

    if (!default_mask) {
        FIBITMAP *mask_tiff = FreeImage_Load(FIF_TIFF, mask_path.c_str(), TIFF_DEFAULT);
        assert(mask_tiff != nullptr);

        mask_width = FreeImage_GetWidth(mask_tiff);
        mask_size = mask_width * mask_width;

        logger.info("Kernel size is %lux%lu\n", mask_width, mask_width);

        mask = get_array_from_bitmap(mask_tiff, float_type);
        FreeImage_Unload(mask_tiff);

    } else {
        switch(float_type) {
            case half_float:
                std::get<single_float>(mask) = new float[mask_size];
                std::fill_n(std::get<single_float>(mask), mask_size, 1.0f / mask_size);
                std::get<half_float>(mask) = float_to_half(std::get<single_float>(mask), mask_size);
                delete[] std::get<single_float>(mask);
                break;
            case single_float:
                std::get<single_float>(mask) = new float[mask_size];
                std::fill_n(std::get<single_float>(mask), mask_size, 1.0f / mask_size);
                break;
            case double_float:
                std::get<double_float>(mask) = new double[mask_size];
                std::fill_n(std::get<double_float>(mask), mask_size, 1.0 / mask_size);
                break;
        }
    }

    FIBITMAP *image_source = FreeImage_Load(FIF_TIFF, image_path.c_str(), TIFF_DEFAULT);
    assert(image_source != nullptr);

    size_t width {FreeImage_GetWidth(image_source)};
    size_t height {FreeImage_GetHeight(image_source)};

    mixed_precision_tuple image_float = get_array_from_bitmap(image_source, float_type);
    FreeImage_Unload(image_source);

    std::string base_path{image_path.substr(0, image_path.length() - 5)};
    std::string golden_path {base_path + "_golden_" + std::to_string(mask_width) + ".tiff"};

    if (golden_mode) {
        std::vector<elapsed_time_t> useless;
        float *image_golden_float = cpu_convolution_naive(std::get<single_float>(image_float), width, height,
                std::get<single_float>(mask), mask_width, useless, 0);

        //golden save
        FIBITMAP *image_golden_tiff = ImageUtils_ArrayToFloat(image_golden_float, width, height);
        FreeImage_Save(FIF_TIFF, image_golden_tiff, golden_path.c_str(), TIFF_DEFAULT);
        FreeImage_Unload(image_golden_tiff);
        delete[] image_golden_float;
        logger.info("Saved %s\n", golden_path.c_str());
    } else {
        mixed_precision_tuple image_test_float;

        for (const auto& impl : impl_map) {
            if (all_mode) {
                implementation = impl.first;
                logger.info("\nUsing implementation %s\n", implementation.c_str());
            }

            std::vector<elapsed_time_t> results;

            switch(float_type) {
                case half_float:
                    std::get<half_float>(image_test_float) = std::get<half_float>(impl_map.at(implementation))(
                            std::get<half_float>(image_float), width, height,
                            std::get<half_float>(mask), mask_width, results, loop_count);
                    break;
                case single_float:
                    std::get<single_float>(image_test_float) = std::get<single_float>(impl_map.at(implementation))(
                            std::get<single_float>(image_float), width, height,
                            std::get<single_float>(mask), mask_width, results, loop_count);
                    break;
                case double_float:
                    std::get<double_float>(image_test_float) = std::get<double_float>(impl_map.at(implementation))(
                            std::get<double_float>(image_float), width, height,
                            std::get<double_float>(mask), mask_width, results, loop_count);
                    break;
            }

            if (results.size() > 0) {
                long min {results[0]}, max {results[0]};
                long mean {0};
                for (auto const& result : results) {
                    if (result > max)
                        max = result;
                    else if (result < min)
                        min = result;
                    mean += result;
                }
                logger.csv("%.8f", (double) mean / loop_count / 1000.);
                logger.info("Results on %lu iterations (in us):\n", results.size());
                logger.info("\tmin:  %.4f\n", (double) min / 1000);
                logger.info("\tmax:  %.4f\n", (double) max / 1000);
                logger.info("\tmean: %.4f\n", (double) mean / loop_count / 1000);
            } else {
                logger.info("No results available.\n");
            }

            if (!all_mode) {
                break;
            }
        }

        if (!profile_only) {
            if (!no_save) {
                FIBITMAP * image_test_tiff = ImageUtils_ArrayToFloat(std::get<single_float>(image_test_float),
                        width, height);
                std::string save_path {base_path + "_" + implementation + "_" + std::to_string(mask_width) + ".tiff"};
                FreeImage_Save(FIF_TIFF, image_test_tiff, save_path.c_str(), TIFF_DEFAULT);
                logger.info("Saved %s\n", save_path.c_str());
                FreeImage_Unload(image_test_tiff);
            }

            FIBITMAP *golden = FreeImage_Load(FIF_TIFF, golden_path.c_str(), TIFF_DEFAULT);
            if (golden == nullptr) {
                logger.info("[WARNING] No golden image found, cannot compare image\n");
            } else {
                float *golden_float = ImageUtils_FloatToArray<float>(golden);
                FreeImage_Unload(golden);

                float *image_error = new float[width * height];
                double global_error = compare_images(std::get<single_float>(image_test_float),
                        golden_float, width, height, image_error);
                FIBITMAP *error_fi = ImageUtils_ArrayToFloat(image_error, width, height);
                std::string error_path {base_path + "_" + implementation + "_" + std::to_string(mask_width) + "_diff.tiff"};
                FreeImage_Save(FIF_TIFF, error_fi, error_path.c_str(), TIFF_DEFAULT);
                delete[] image_error;

                logger.csv(",%.8f", global_error);
                logger.info("Global difference (normalized) is %.4f\n", global_error);

                delete[] golden_float;
            }
        }
    }

    delete[] std::get<half_float>(image_float);
    delete[] std::get<single_float>(image_float);
    delete[] std::get<double_float>(image_float);

    FreeImage_DeInitialise();

    logger.csv("\n");
    logger.info("Program successful\n");

    cudaDeviceReset();
    return EXIT_SUCCESS;
}

mixed_precision_tuple get_array_from_bitmap(FIBITMAP *image_source, float_precision float_type)
{
    mixed_precision_tuple image_float;

    const size_t width {FreeImage_GetWidth(image_source)};
    const size_t height {FreeImage_GetHeight(image_source)};

    switch (float_type) {
        case half_float:
            std::get<single_float>(image_float) = ImageUtils_FloatToArray<float>(image_source);
            std::get<half_float>(image_float) = float_to_half(std::get<single_float>(image_float), width * height);
            delete[] std::get<single_float>(image_float);
            std::get<single_float>(image_float) = nullptr;
            break;
        case single_float:
            std::get<single_float>(image_float) = ImageUtils_FloatToArray<float>(image_source);
            break;
        case double_float:
            std::get<double_float>(image_float) = ImageUtils_FloatToArray<double>(image_source);
            break;
    }

    return image_float;
}

void promote_to_double(mixed_precision_tuple *image_test_float, size_t width, size_t height, float_precision float_type)
{
    switch (float_type) {
        case half_float:
            std::get<double_float>(*image_test_float) = half_to_double(std::get<half_float>(*image_test_float),
                    width * height);
            delete[] std::get<half_float>(*image_test_float);
            std::get<half_float>(*image_test_float) = nullptr;
            break;
        case single_float:
            std::get<double_float>(*image_test_float) = float_to_double(std::get<single_float>(*image_test_float),
                    width * height);
            delete[] std::get<single_float>(*image_test_float);
            std::get<single_float>(*image_test_float) = nullptr;
            break;
        case double_float:
            break;
    }
}
