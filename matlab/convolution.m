function y = convolution
image_path = '../images/cameraman';
kernel_radius = 5;
loop_count = 100;

h_image_in = single(imread([image_path '.png']));

kernel_width = 2 * kernel_radius + 1;
h_kernel = ones(kernel_width, kernel_width, 'single') / (kernel_width * kernel_width);

d_image_in = gpuArray(h_image_in);
d_kernel = gpuArray(h_kernel);

conv2(d_image_in, d_kernel, 'same');
results = zeros(loop_count, 1);
for i=1:loop_count
    tic
    d_image_out = conv2(d_image_in, d_kernel, 'same');
    results(i) = toc;
end
results = results .* 1000000;
disp(['min:  ' num2str(min(results))]);
disp(['max:  ' num2str(max(results))]);
disp(['mean: ' num2str(mean(results))]);

h_image_out = uint8(gather(d_image_out));
image_out_path = [image_path '_matlab_' num2str(kernel_radius) '.png'];

disp(['Writing image to ' image_out_path]);
imwrite(h_image_out, image_out_path);
y = 0;
