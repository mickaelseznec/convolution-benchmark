#Convolution benchmark

Required dependencies:
- Arrayfire: [https://arrayfire.com/download/]
- cuDNN (must create an Nvidia account): [https://developer.nvidia.com/cudnn]
Make sure they are up to date with your current CUDA version.

This benchmark also use the pre-included libraries:
- FreeImage
- Docopt

To build the project:
- go to cpp/
- source load\_environment.sh
- make
